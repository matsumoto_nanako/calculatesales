package jp.alhinc.matsumoto_nanako.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>(); //支店コードと支店名
		Map<String, Long> branchSales = new HashMap<>(); //支店コードと売上金額
		Map<String, String> commodityNames = new HashMap<>(); //商品コードと商品名
		Map<String, Long> commoditySales = new HashMap<>(); //商品コードと売上金額

		//コマンドライン引数が渡されているか確認する
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

        //支店定義ファイルの読み込み
		if(!input(args[0], "branch.lst", "支店", "^\\d{3}$", branchNames, branchSales)) {
			return;
		}

		//商品定義ファイルの読み込み
		if(!input(args[0], "commodity.lst", "商品", "^[A-Za-z0-9]{8}+$", commodityNames, commoditySales)){
			return;
		}

		//売上ファイルの読み込み
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//対象がファイルであり、「数字8桁.rcd」なのかを判定
		for(int i = 0; i< files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//OS問わず正常動作するためにListをソート
		Collections.sort(rcdFiles);

		//売上ファイルが連番か確認する
		for(int i = 0; i < rcdFiles.size() - i; i++) {
			//比較する2つのファイル名の先頭から8文字を切り出し、int型に変換
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売上ファイルの中身を抽出
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				List <String> rcdData = new ArrayList<>(); //支店コードと売上金額をリスト化

				//売上ファイルの中身を支店コードと商品コードと売上金額のセットでリストに追加
				String line;
				while((line = br.readLine()) != null) {
					rcdData.add(line);
				}

				//売上ファイルのフォーマット確認
				if(rcdData.size() != 3) {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在するかを確認
				if(!branchNames.containsKey(rcdData.get(0))){
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}
				//売上ファイルの商品コードが商品定義ファイルに存在するかを確認
				if(!commodityNames.containsKey(rcdData.get(1))){
					System.out.println(files[i].getName() + "の商品コードが不正です");
					return;
				}

				//売上金額が数字なのかを確認
				if(!rcdData.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long fileSale = Long.parseLong(rcdData.get(2)); //売上金額を取得してロング型に変換
				Long saleAmount = branchSales.get(rcdData.get(0)) + fileSale; //該当する支店の合計金額に加算
				Long commodityAmount = commoditySales.get(rcdData.get(1)) + fileSale; //該当する商品の合計金額に加算

				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(commodityAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(rcdData.get(0), saleAmount); //支店コードを保持するMapに売上金額を追加
				commoditySales.put(rcdData.get(1), commodityAmount);//商品コードを保持するMapに売上金額を追加

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//支店別集計ファイルの作成
		if(!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイルの作成
		if(!output(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}
	//inputメソッド作成
	private static boolean input(String filePath, String fileName, String difFile, String regex, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, fileName);
			if(!file.exists()) {
				System.out.println(difFile + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (! items[0].matches(regex))){
					System.out.println(difFile + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		} return true;
	}

	//outputメソッド作成
	private static boolean output(String filePath, String fileName, Map<String, String> names, Map<String, Long> sales) {

		File file = new File(filePath, fileName);
		BufferedWriter bw = null;
		try {
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
